package br.com.concretesolutions.githubrepositories;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import br.com.concretesolutions.githubrepositories.dagger.component.DaggerNetComponent;
import br.com.concretesolutions.githubrepositories.dagger.component.NetComponent;
import br.com.concretesolutions.githubrepositories.dagger.module.AppModule;
import br.com.concretesolutions.githubrepositories.dagger.module.NetModule;

/**
 * Created by Rafael on 17-Dec-16.
 */

public class App extends Application {
    public static final String GITHUB_BASEURL = "https://api.github.com";

    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Fresco.initialize(this);

        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(GITHUB_BASEURL))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}
