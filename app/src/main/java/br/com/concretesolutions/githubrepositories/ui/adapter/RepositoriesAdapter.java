package br.com.concretesolutions.githubrepositories.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import br.com.concretesolutions.githubrepositories.R;
import br.com.concretesolutions.githubrepositories.domain.repository.Item;
import br.com.concretesolutions.githubrepositories.domain.repository.Repositories;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Rafael on 15-Dec-16.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {

    private Repositories mRepositories;
    private Context mContext;

    public RepositoriesAdapter(Context context, Repositories repositories){
        mRepositories = repositories;
        mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_user_avatar) SimpleDraweeView mUserAvatar;
        @BindView(R.id.txt_username) TextView mUsername;
        @BindView(R.id.txt_repo_name) TextView mRepoName;
        @BindView(R.id.txt_repo_desc) TextView mRepoDesc;
        @BindView(R.id.txt_fork_count) TextView mForkCount;
        @BindView(R.id.txt_star_count) TextView mStarCount;

        public ViewHolder(final View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View repositoryView = inflater.inflate(R.layout.item_repository, parent, false);
        ViewHolder viewHolder = new ViewHolder(repositoryView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item repo = mRepositories.getItems().get(position);

        holder.mUserAvatar.setImageURI(Uri.parse(repo.getOwner().getAvatarUrl()));
        holder.mUsername.setText(repo.getOwner().getLogin());
        holder.mRepoName.setText(repo.getName());
        holder.mRepoDesc.setText(repo.getDescription());
        holder.mForkCount.setText(repo.getForksCount().toString());
        holder.mStarCount.setText(repo.getStargazersCount().toString());

    }

    @Override
    public int getItemCount() {
        return mRepositories.getItems().size();
    }

    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public void addAll(List<Item> data) {
        mRepositories.getItems().addAll(data);
        notifyDataSetChanged();
    }
}
