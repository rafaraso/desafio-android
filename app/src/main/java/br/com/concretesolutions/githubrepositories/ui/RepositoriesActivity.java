package br.com.concretesolutions.githubrepositories.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import br.com.concretesolutions.githubrepositories.App;
import br.com.concretesolutions.githubrepositories.R;
import br.com.concretesolutions.githubrepositories.domain.GithubAPI;
import br.com.concretesolutions.githubrepositories.domain.repository.Repositories;
import br.com.concretesolutions.githubrepositories.ui.adapter.RepositoriesAdapter;
import br.com.concretesolutions.githubrepositories.ui.utils.EndlessRecyclerViewScrollListener;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RepositoriesActivity extends AppCompatActivity {

    @Inject
    Retrofit retrofit;

    @Nullable
    @BindView(R.id.rv_repositories)
    RecyclerView rvRepositories;

    private RepositoriesAdapter adapter;

    private EndlessRecyclerViewScrollListener scrollListener;

    private int mPage;
    private Repositories mRepositories;
    private LinearLayoutManager linearLayoutManager;

    private Parcelable parce;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        ((App) getApplication()).getNetComponent().inject(this);
        ButterKnife.bind(this);

        linearLayoutManager = new LinearLayoutManager(this);
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        rvRepositories.addOnScrollListener(scrollListener);

        getData(mPage);
    }

    private void getData(int page) {
        Call<Repositories> call =  retrofit.create(GithubAPI.class).getRepos(page);
        call.enqueue(new Callback<Repositories>() {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    mRepositories = response.body();
                    if (adapter == null) {
                        adapter = new RepositoriesAdapter(getApplicationContext(), mRepositories);
                        rvRepositories.setAdapter(adapter);
                        rvRepositories.setLayoutManager(linearLayoutManager);
                    } else {
                        adapter.addAll(mRepositories.getItems());
                    }
                    adapter.setOnItemClickListener(new RepositoriesAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View v, int position) {
                            TextView username = ButterKnife.findById (v, R.id.txt_username);
                            TextView repo = ButterKnife.findById (v, R.id.txt_repo_name);
                            Intent intent = new Intent(v.getContext(), PullsActivity.class);
                            intent.putExtra(PullsActivity.KEY_OWNER, username.getText().toString());
                            intent.putExtra(PullsActivity.KEY_REPO, repo.getText().toString());
                            startActivity(intent);
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<Repositories> call, Throwable t) {
                //TODO: LOG
            }
        });

    }

    public void loadNextDataFromApi(int page) {
        getData(mPage+=page);
    }
}