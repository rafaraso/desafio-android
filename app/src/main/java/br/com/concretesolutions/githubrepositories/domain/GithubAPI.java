package br.com.concretesolutions.githubrepositories.domain;

import java.util.List;


import br.com.concretesolutions.githubrepositories.domain.pullrequest.PullRequest;
import br.com.concretesolutions.githubrepositories.domain.repository.Repositories;
import br.com.concretesolutions.githubrepositories.domain.repository.SingleUser;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rafael on 15-Dec-16.
 */

public interface GithubAPI {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Call<Repositories> getRepos(@Query("page") int page);

    @GET("/users/{username}")
    Call<SingleUser> getUser(@Path("user") String username);

    @GET("/repos/{user}/{repository}/pulls")
    Call<List<PullRequest>> getPulls(
            @Path("user") String user,
            @Path("repository") String repository);

}
