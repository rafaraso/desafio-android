package br.com.concretesolutions.githubrepositories.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.com.concretesolutions.githubrepositories.R;
import br.com.concretesolutions.githubrepositories.domain.pullrequest.PullRequest;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rafael on 16-Dec-16.
 */

public class PullsAdapter extends RecyclerView.Adapter<PullsAdapter.ViewHolder> {

    private Context mContext;
    private List<PullRequest> mPulls;

    public PullsAdapter(Context context, List<PullRequest> pulls) {
        mContext = context;
        mPulls = pulls;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_pull_user_avatar) SimpleDraweeView mUserAvatar;
        @BindView(R.id.txt_pull_username) TextView mUsername;
        @BindView(R.id.txt_pull_title) TextView mPullTitle;
        @BindView(R.id.txt_pull_body) TextView mPullBody;
        @BindView(R.id.txt_pull_date) TextView mPullDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View pullView = inflater.inflate(R.layout.item_pull, parent, false);
        ViewHolder viewHolder = new ViewHolder(pullView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        PullRequest pull = mPulls.get(position);

        holder.mUserAvatar.setImageURI(Uri.parse(pull.getUser().getAvatarUrl()));
        holder.mUsername.setText(pull.getUser().getLogin());
        holder.mPullTitle.setText(pull.getTitle());
        holder.mPullTitle.setTag(pull.getHtmlUrl());//URL FOR EXTERNAL LINK
        holder.mPullBody.setText(pull.getBody());
        holder.mPullDate.setText(formatDate(pull.getCreatedAt()));
    }

    @Override
    public int getItemCount() {
        return mPulls.size();
    }

    String formatDate(String time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
        Date d = null;
        try {
            d = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(d);

    }
}
