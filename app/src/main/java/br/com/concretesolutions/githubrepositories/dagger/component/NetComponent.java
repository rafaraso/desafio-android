package br.com.concretesolutions.githubrepositories.dagger.component;

import javax.inject.Singleton;

import br.com.concretesolutions.githubrepositories.dagger.module.AppModule;
import br.com.concretesolutions.githubrepositories.dagger.module.NetModule;
import br.com.concretesolutions.githubrepositories.ui.PullsActivity;
import br.com.concretesolutions.githubrepositories.ui.RepositoriesActivity;
import dagger.Component;

/**
 * Created by Rafael on 17-Dec-16.
 */

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent {

    void inject (RepositoriesActivity activity);
    void inject (PullsActivity activity);

}
