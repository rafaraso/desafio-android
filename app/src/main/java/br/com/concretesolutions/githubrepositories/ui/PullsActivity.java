package br.com.concretesolutions.githubrepositories.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import br.com.concretesolutions.githubrepositories.App;
import br.com.concretesolutions.githubrepositories.R;
import br.com.concretesolutions.githubrepositories.domain.GithubAPI;
import br.com.concretesolutions.githubrepositories.domain.pullrequest.PullRequest;
import br.com.concretesolutions.githubrepositories.ui.adapter.PullsAdapter;
import br.com.concretesolutions.githubrepositories.ui.utils.ItemClickSupport;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PullsActivity extends AppCompatActivity {

    public static final String KEY_OWNER = "br.com.concretesolutions.desafioconcrete.OWNER";
    public static final String KEY_REPO = "br.com.concretesolutions.desafioconcrete.REPO";

    @Inject
    Retrofit retrofit;

    @Nullable
    @BindView(R.id.rv_pulls)
    RecyclerView rvPulls;

    private PullsAdapter mPullsAdapter;

    private List<PullRequest> mPulls;
    private String mUser;
    private String mRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulls);
        ((App) getApplication()).getNetComponent().inject(this);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(KEY_OWNER) && getIntent().hasExtra(KEY_REPO)) {
            mUser = getIntent().getStringExtra(KEY_OWNER);
            mRepo = getIntent().getStringExtra(KEY_REPO);
        } else {
            throw new IllegalArgumentException("Activity cannot find  extras " + KEY_OWNER + KEY_REPO);
        }

        Call<List<PullRequest>> call = retrofit.create(GithubAPI.class).getPulls(mUser, mRepo);
        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    mPulls = response.body();
                    mPullsAdapter = new PullsAdapter(getApplicationContext(),mPulls);
                    rvPulls.setAdapter(mPullsAdapter);
                    rvPulls.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });

        ItemClickSupport.addTo(rvPulls).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        TextView pullTitleTextView = ButterKnife.findById(v, R.id.txt_pull_title);
                        String pullRequestURL = pullTitleTextView.getTag().toString();
                        Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequestURL));
                        startActivity(intent);
                    }
                }
        );
    }
}
